FROM openjdk

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

COPY ./target /usr/src/app

EXPOSE 8080

ENTRYPOINT java -jar stephanie-devops-essentials-assignment1.0-SNAPSHOT.jar
