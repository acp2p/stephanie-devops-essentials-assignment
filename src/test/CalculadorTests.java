package test;

import factorial.java.Calculador;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculadorTests {

    @Test
    public void calculaFatorialTest() {
        Integer value = 720;
        assertEquals(value, Calculador.calculaFatorial(6));
    }

}
