package factorial.java;

public class Calculador {

    public static Integer calculaFatorial(Integer n) {

        Integer f = n;

        System.out.println("Cálculo Fatorial... /n----");
        while (n > 1){

            f = f * (n - 1);
            n--;

            System.out.println("/n" + f);
        }

        System.out.println("----");
        return f;
    }

}
