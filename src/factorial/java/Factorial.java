package factorial.java;

public class Factorial {

    private static final Integer NUMBER_1 = 69;

    public void main(String[] args){

        Integer resultado = Calculador.calculaFatorial(NUMBER_1);

        System.out.println("Resultado do Fatorial de " + NUMBER_1 + " é: " + resultado);
    }
}
